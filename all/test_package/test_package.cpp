#include "tiledb/tiledb"

int main() {
    tiledb::Context ctx;
    std::string array_name("quickstart_dense_array");
    tiledb::Object::object(ctx, array_name).type();

    return 0;
}

from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import get, copy, rmdir
from conan.tools.build import check_min_cppstd
from conan.tools.scm import Version
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.env import VirtualBuildEnv
import os

required_conan_version = ">=1.53.0"

class TiledbConan(ConanFile):
    name = "tiledb"
    description = "The Universal Storage Engine"
    license = "MIT"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://github.com/TileDB-Inc/TileDB"
    topics = ("storage-engine", "sparse-data", "s3", "data-analysis")
    package_type = "library"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "with_memfs": [True, False],
        "with_tbb": [True, False],
        "with_serialization": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "with_memfs": False,
        "with_tbb": False,
        "with_serialization": False,
    }

    @property
    def _min_cppstd(self):
        return 20

    @property
    def _compilers_minimum_version(self):
        return {
            "gcc": "11",
            "clang": "12",
            "apple-clang": "13",
            "Visual Studio": "16",
            "msvc": "192",
        }

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self, src_folder="src")

    def requirements(self):
        self.requires("libmagic/5.45", transitive_headers=True, transitive_libs=True)
        self.requires("bzip2/1.0.8")
        self.requires("lz4/1.9.4")
        self.requires("spdlog/1.12.0")
        self.requires("zlib/1.2.13")
        self.requires("zstd/1.5.5")
        self.requires("libwebp/1.3.1")

        if self.options.with_serialization:
            self.requires("capnproto/0.10.3")
            self.requires("libcurl/8.2.0")

        if self.settings.os != "Windows":
            self.requires("openssl/[>=1.1 <4]")

    def build_requirements(self):
        self.tool_requires("cmake/[>=3.21.0 <4]")

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, self._min_cppstd)
        minimum_version = self._compilers_minimum_version.get(str(self.settings.compiler), False)
        if minimum_version and Version(self.settings.compiler.version) < minimum_version:
            raise ConanInvalidConfiguration(
                f"{self.ref} requires C++{self._min_cppstd}, which your compiler does not support."
            )
        # FIXME: support tbb. tbb cci's recipe doesn't supprt conan v2(yet).
        if self.options.with_tbb:
            raise ConanInvalidConfiguration(f"{self.ref} doesn't support tbb (yet).")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["TILEDB_VCPKG"] = True
        tc.cache_variables["TILEDB_SUPERBUILD"] = False
        tc.cache_variables["TILEDB_FORCE_ALL_DEPS"] = True
        tc.cache_variables["TILEDB_MEMFS"] = self.options.with_memfs
        tc.cache_variables["TILEDB_TBB"] = self.options.with_tbb
        tc.cache_variables["TILEDB_TBB_SHARED"] = False
        tc.cache_variables["TILEDB_STATIC"] = not self.options.shared
        tc.cache_variables["TILEDB_SERIALIZATION"] = self.options.with_serialization
        tc.cache_variables["TILEDB_TESTS"] = False
        tc.cache_variables["TILEDB_WERROR"] = False
        tc.cache_variables["TILEDB_EP_BASE"] = os.path.join(self.build_folder, "external").replace("\\", "/")
        # overwrite "libmagic_DICTIONARY" which is defined at cmake/Modules/FindLibMagic.cmake.
        tc.cache_variables["libmagic_DICTIONARY"] = os.path.join(self.dependencies["libmagic"].package_folder, "res", "magic.mgc").replace("\\", "/")
        tc.generate()
        dpes = CMakeDeps(self)
        dpes.generate()
        venv = VirtualBuildEnv(self)
        venv.generate(scope="build")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)
        cmake = CMake(self)
        cmake.install()

        rmdir(self, os.path.join(self.package_folder, "lib", "cmake"))
        rmdir(self, os.path.join(self.package_folder, "lib", "pkgconfig"))

    def package_info(self):
        self.cpp_info.libs = ["tiledb"]

        self.cpp_info.set_property("cmake_file_name", "TileDB")
        if (self.options.shared):
            self.cpp_info.set_property("cmake_target_name", "TileDB::TileDB_shared")
        else:
            self.cpp_info.set_property("cmake_target_name", "TileDB::TileDB_static")
        self.cpp_info.set_property("pkg_config_name", "TileDB")
